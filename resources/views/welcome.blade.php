<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Example</title>
</head>
<body>
<script src="{{ asset('css/app.css') }}"></script>
  <div id="app"></div>

  <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>