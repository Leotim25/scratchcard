import { Howl, Howler } from "howler"

export default {
    setBalance(state, num) {
        state.balance = num //改變成state.js定義的count數值去加1，num是在actions我們丟進去的1
    },
    setUserData(state, user){
        state.userData = user
    },
    setWalletData(state, wallet) {
        state.walletData = wallet
    },
    addShoppingCartItem(state, item) {
        state.shoppingCart.push(item)
        //state.groupsData.unshift(group)
    }, 
    removeShoppingCartItem(state, item_id) {

        for (let i = 0; i < state.shoppingCart.length; i++) {
            if(state.shoppingCart[i].sn == item_id){

                state.shoppingCart.splice(i, 1);
            } 
        }

    },
    resetShoppingCart(state) {
        state.shoppingCart = [];
    },
    setJWTToken(state, token) {
        state.jwtToken = token
    },

    playBackgroundAudio(state){

        if(state.backgroundAudio==''){
            state.backgroundAudio = new Howl({
                // 參數設定[註1]
                    src: [ 'audio/backgroud.mp3' ],
                    autoplay: true, 
                    loop: true, 
                    volume: 0.5,
             });
             state.backgroundAudio.play();
             state.backgroundAudioStatus = !state.backgroundAudioStatus;

        }else{
            state.backgroundAudio.stop();
            state.backgroundAudio.play();
            state.backgroundAudioStatus = !state.backgroundAudioStatus;
        }
        window.addEventListener( 'blur', function() { 
            console.log("blur and stop");
            state.backgroundAudio.stop(); 
            state.backgroundAudioStatus = !state.backgroundAudioStatus;
        } );
        window.addEventListener( 'focus', function() { 
            console.log("focus and play");
            state.backgroundAudio.play(); 
            state.backgroundAudioStatus = !state.backgroundAudioStatus;
        } );
             
    },
    stopBackgroundAudio(state){
        console.log('stopBackgroundAudio');
        state.backgroundAudio.stop();
        state.backgroundAudioStatus = !state.backgroundAudioStatus;
        
    }
}