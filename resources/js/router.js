import Vue from 'vue'
import Router from 'vue-router'
import GameView from './views/GameView.vue'
import CardMenu from './views/CardMenu.vue'
import Lobby from './views/Lobby.vue'
import Login from './views/Login.vue'
import Result from './views/Result.vue'
import Transfer from './views/Transfer.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/Transfer',
      name: 'Transfer',
      component: Transfer
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/GameView',
      name: 'GameView',
      component: GameView
    },
    {
      path: '/CardMenu',
      name: 'CardMenu',
      component: CardMenu
    },
    {
      path: '/Lobby',
      name: 'Lobby',
      component: Lobby
    },
    {
      path: '/',
      name: 'Index',
      redirect: { name: 'Login' }
    },
    {
      path: '/Result',
      name: 'Result',
      component: Result
    }
  ]
})
