<?php

use Illuminate\Http\Request;
use App\Http\Middleware\CheckU168Auth;
use App\Http\Middleware\RefreshToken;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */
Route::prefix('v1')->group(function () {
    Route::get('/periods/class',[
        //'middleware'  => RefreshToken::class,
        'uses' => 'PeriodsController@class'
    ]);
    Route::get('/periods/list',[
        //'middleware'  => RefreshToken::class,
        'uses' => 'PeriodsController@list'
    ]);
    Route::get('/periods/remaining',[
        //'middleware'  => RefreshToken::class,
        'uses' => 'PeriodsController@remaining'
    ]);
    Route::get('/periods/odds',[
        //'middleware'  => RefreshToken::class,
        'uses' => 'PeriodsController@odds'
    ]);
    Route::get('/periods/piece',[
        //'middleware'  => RefreshToken::class,
        'uses' => 'PeriodsController@piece'
    ]);
    Route::post('/scratch/checkout',[
        //'middleware'  => [RefreshToken::class, CheckU168Auth::class],
        'middleware'  => RefreshToken::class,
        'uses' => 'ScratchController@checkout'
    ]);
    Route::get('/member/products',[
        'middleware'  => [RefreshToken::class],
        'uses' => 'MemberController@products'
    ]);
    Route::post('/member/productsResult',[
        'middleware'  => [RefreshToken::class],
        'uses' => 'MemberController@productsResult'
    ]);
    Route::prefix('u168')->group(function () {
        Route::post('/login/exchange',[
            //'middleware'  => [RefreshToken::class, CheckU168Auth::class],
            'uses' => 'U168\LoginController@exchange'
        ]);
        Route::get('/login/balace',[
            //'middleware'  => [RefreshToken::class, CheckU168Auth::class],
            'uses' => 'U168\LoginController@balace'
        ]);
        
        Route::get('/login/checkout',[
            //'middleware'  => [RefreshToken::class, CheckU168Auth::class],
            'uses' => 'U168\LoginController@checkout'
        ]);
        
        Route::post('/login/auth',[
            'middleware'  => RefreshToken::class,
            'uses' => 'U168\LoginController@auth'
        ]);
    });
        
});