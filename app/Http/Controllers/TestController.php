<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Repositories\ScratchRepository;
use App\Repositories\PeriodsRepository;

class TestController extends Controller
{
    /**
     */
    protected $scratch;
    
    /**
     * TestController constructor.
     * @param $posts
     */
    public function __construct(ScratchRepository $scratch)
    {
        $this->scratch = $scratch;
        //exit;
    }
    public function test1(){
        $this->scratch->periods_id = 1; //期數
        $this->scratch->price = 10; //面額
        $this->scratch->num = 100; //發行幾本
        $this->scratch->limit = 100; //一本幾張
        $this->scratch->odds =array( //獎品列表
            10000=>999,
            1000=>1,
            100=>5,
            50=>10,
            
        );
        $this->scratch->mapping = 2; //開出的對應數量
        $this->scratch->pic = 10; //幾張圖片
        $test = $this->scratch->create();
        print_R($test);
    }
    public function test2(){
        $point = $this->scratch->createPoint(7,2);
        $out = $this->scratch->point2Array($point,10);
        print_R($point);
        print_R($out);
    }
}