<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\ScratchRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ScratchController extends Controller
{
    /**
     */
    protected $scratch;
    /**
     * TestController constructor.
     * @param $posts
     */
    public function __construct(ScratchRepository $scratch )
    {
        $this->scratch = $scratch;
        //exit;
    }
    public function checkout(Request $request){
        $all = $request->all();
        if(isset($all['user_id']) && !empty($all['ids'])){
            $user_id = $all['user_id'];
            $ids = $all['ids'];
            $res = $this->scratch->checkout($ids , $user_id);
            if($res){
                $u168res = false;
                $sn = array();
                $getItem = $this->scratch->productsByIds($res[1]);
                $noGetItem = $this->scratch->productsByIds($res[0]);
                if($getItem){
                    //$u168res = $this->scratch->checkoutByU168($getItem,$all);//這個很重要
                    $u168res = true;
                }
                if($noGetItem){
                    foreach($noGetItem as $value){
                        $sn[] = $value['sn'];
                    }
                }
                if($u168res){
                    $price = 0;
                    foreach ($getItem as $item){
                        $price = $price+$item['period']['price'];
                    }
                    return Response::json(array(
                        'status'      =>  true,
                        'data' => array(
                            'price' =>$price,
                            'buyFew' => count($getItem),
                            'noGetItem'=>implode(",", $sn),
                        ),
                    ), 200);
                    
                }else{
                    if($getItem){
                        foreach($getItem as $id =>$item){
                            $this->scratch->recovery($id);
                        }
                        return Response::json(array(
                            'status'      =>  false,
                            'msg'=>'U168 Error',
                        ), 500);
                    }
                    if($noGetItem){
                        return Response::json(array(
                            'status'      =>  true,
                            'data' => array(
                                'price' =>0,
                                'buyFew' => 0,
                                'noGetItem'=>implode(",", $sn),
                            ),
                        ), 200);
                    }
                }
            }
        }
        return Response::json(array(
            'status'      =>  false,
            'msg'=>'Checkout Error',
            'input'=>$all,
        ), 500);
    }
}