<?php 
namespace App\Http\Controllers\U168;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Repositories\GameLoginRepository;
use App\Repositories\WalletRepository;

class LoginController extends Controller
{
    /**
     */
    protected $login;
    protected $wallet;
    
    /**
     * TestController constructor.
     * @param $posts
     */
    public function __construct(GameLoginRepository $login , WalletRepository $wallet)
    {
        $this->login = $login;
        $this->wallet = $wallet;
    }
    
    public function checkout(Request $request){
        $all = $request->all();
        $res = $this->wallet->walletCheckOut($all);
        if($res){
            return Response::json(array(
                'status' => true,
                'data' => $res,
            ), 200);
        }
        return Response::json(array(
            'status'      =>  false,
        ), 500);
    }
    
    public function fake(Request $request){
        $all = $request->all();
        if($all){
            $login = new GameLoginRepository;
            $res = $login->FakeLoginLocal("xgame_test", "123456");
            return Response::json($res, 200);
        }
        return Response::json(array(
            'status'      =>  false,
        ), 500);
    }
    
    public function exchange(Request $request){
        $all = $request->all();
        if (isset($all['coin_type']) && $all['coin_type'] != "") {
            if (isset($all['coin_amount']) && $all['coin_amount'] != "") {
                //$res = $this->wallet->walletExchangeAck($all);
                $res = true;//fake_local
                if($res){
                    return Response::json(array(
                        'status' => true,
                        'cmd' => 8001021,
                        'data' => $res,
                    ), 200);
                }
            }
         }
        return Response::json(array(
            'status'      =>  false,
            'msg' =>'exchange error',
        ), 500);
    }
    
    public function balace(Request $request){
        $all = $request->all();
        $res = $this->wallet->walletBalaceAck($all);
        if($res){
            return Response::json(array(
                'status' => true,
                'cmd' => 8001022,
                'data' => $res,
            ), 200);
        }
        return Response::json(array(
            'status'      =>  false,
            'msg' =>'balace error',
        ), 500);
    }
    public function auth(Request $request){
        
        $all = $request->all();
        if(isset($all['token']) && $all['token']!=""){
                $guid = guid();
                $input = array();
                $input['token'] = $all['token'];
                $input['game_id'] = env("GAME_ID");
                $input['conn_id'] = $guid;
                $res = $this->login->AuthAck($input);
                if($res){
                    $access_token = _encrypt($guid."|".date("Y-m-d")."|".$all['token']."|".$res['game_token']."|".$res['user_id'],"E",env("JWT_SECRET"));
                    return Response::json(array(
                        'status' => true,
                        'cmd' => 8001001,
                        'data' => $res,
                        'access_token' => $access_token,
                    ), 200);
                }
        }
        return Response::json(array(
            'status'      =>  false,
            'msg' =>'auth error',
        ), 500);
    }
}