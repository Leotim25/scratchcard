<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Repositories\MemberRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class MemberController extends Controller
{
    protected $memberRepository;
    /**
     * TestController constructor.
     * @param $posts
     */
    public function __construct(MemberRepository $memberRepository )
    {
        $this->memberRepository = $memberRepository;
        //exit;
    }
    public function productsResult(Request $request){
        //productResult
        $all = $request->all();
        if(isset($all['user_id']) && $all['user_id'] != ""){
            if(isset($all['ids']) && !empty($all['ids'])){
                $res = $this->memberRepository->productsResult($all['user_id'],$all['ids']);
            }else{
                $res = $this->memberRepository->productsResult($all['user_id']);
            }
            if($res){
                return Response::json(array(
                    'status'      =>  true,
                    'data'   =>  $res
                ), 200);
            }
        }
        return Response::json(array(
            'status'      =>  false
        ), 500);
    }
    public function products(Request $request){
        $all = $request->all();
        if(isset($all['user_id']) && $all['user_id'] != ""){
            $res = $this->memberRepository->products($all['user_id']);
            if($res){
                return Response::json(array(
                    'status'      =>  true,
                    'data'   =>  $res
                ), 200);
            }
        }
        return Response::json(array(
            'status'      =>  false
        ), 500);
    }
}