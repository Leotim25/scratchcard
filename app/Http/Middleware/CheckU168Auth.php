<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Response;
use Closure;

class CheckU168Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $ulg_api;
    public function __construct()
    {
        // $url, $params = false, $ispost = 0, $https = 0
        $this->ulg_api = env('ULG168_API_URL');
    }
    
    public function handle($request, Closure $next)
    {
        $all = $request->all();
        if (isset($all['game_token']) && $all['game_token'] != "" && isset($all['token']) && $all['token'] != "") {
            $url = $this->ulg_api . "/v1/apis/ulg168/auth/validate";
            $res = curl($url, $all, "POST", 1);
            if ($res['httpCode'] == 200 ) {
                $response = json_decode($res['response'], true);
                if($response['status'] == 1){
                    return $next($request);
                }
            }
        }
        return Response::json(array(
            'status'      =>  false,
            'msg'=>'CheckU168Error',
        ), 500);
    }
}
