<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scratch extends Model
{
    //
    protected $table = 'scratchs';
    
    public function period()
    {
        return $this->belongsTo('App\Models\Period','periods_id', 'id');
    }
    
}
