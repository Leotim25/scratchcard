<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    //
    
    public function scratchs()
    {
        return $this->hasMany('App\Models\Scratch', 'periods_id','id');
    }
}
