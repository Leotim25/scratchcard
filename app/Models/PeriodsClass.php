<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class PeriodsClass extends Model
{
    protected $table = 'periods_class';
    
    public function period()
    {
        return $this->belongsTo('App\Models\Period','periods_class_id','periods_class_id')->where('status',1)->select('id as periods_id','sn','price','periods_class_id','max_award');
    }
}
