<?php 
namespace App\Repositories;

class WalletRepository
{

    protected $ulg_api;

    public function __construct()
    {
        $this->ulg_api = env('ULG168_API_URL');
    }

    public function walletExchangeAck($data)
    { 
        
        return true;

    }

    public function walletBalaceAck($data){

        $url = $this->ulg_api . "/v1/apis/ulg168/wallet/balance";
        $res = curl($url,$data,"GET",1);
        if ($res['httpCode'] == 200) {
            return json_decode($res['response'], true);
        }
        return false;
    }


    public function walletCheckOut($data){

        $url = $this->ulg_api . "/v1/apis/ulg168/checkout";

        $res = curl($url,$data,"POST",1);

        if ($res['httpCode'] == 200) {
            return json_decode($res['response'], true);
        }
        return false;

    }

} 
