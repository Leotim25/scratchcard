<?php
namespace App\Repositories;

class GameLoginRepository
{

    protected $ulg_api;

    public function __construct()
    {
        // $url, $params = false, $ispost = 0, $https = 0
        $this->ulg_api = env('ULG168_API_URL');
    }

    public function AuthAck($data)
    {
        if (isset($data['token']) && $data['token'] != "") {
            if (isset($data['game_id']) && $data['game_id'] != "") {
                if (isset($data['conn_id']) && $data['conn_id'] != "") {
                    $url = $this->ulg_api . "/v1/apis/ulg168/auth";
                    $res = curl($url, $data, "POST", 1);
                    if ($res['httpCode'] == 200) {
                        return json_decode($res['response'], true);
                    }
                }
            }
        }
        return false;
    }
    public function FakeLogin($username,$password)
    {
        $url = $this->ulg_api . "/v1/apis/ulg168/login";
        $data = array();
        $data['username']=$username;
        $data['password']=$password;
        $res = curl($url, $data, "POST", 1);
        if ($res['httpCode'] == 200) {
            return json_decode($res['response'], true);
        }
        return false;
    }
    public function FakeLoginLocal($username,$password)
    {
        $temp = array();
        $temp['result'] = 1;
        $temp['token']="FAKE";
        $temp['game_token']="TEST";
        $temp['user_id'] = 123456;
        //$authorization = _encrypt($guid."|".date("Y-m-d")."|".$input['token']."|".$res['game_token']."|".$res['user_id'],"E",env("JWT_SECRET"));
        return $temp;
    }
} 
